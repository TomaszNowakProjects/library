Application loads informations from book.json file and unable to get inforations about books and authors rating via restful endpoints.

uri to book details: http://localhost:8080/bookLibrary/books/"isbn"
e.g. http://localhost:8080/bookLibrary/books/9788324677610

uri to books category: http://localhost:8080/bookLibrary/books?category="categoryName"
e.g. http://localhost:8080/bookLibrary/books?category=Computers

uri to authors rating: http://localhost:8080/bookLibrary/authors/rating


To build the project use following command: mvn clean install

After building the application run following command to start it: mvn tomcat7:run

Framework: JavaEE, json serialize/deserialize - Jackson

Testing framework: Junit4

Design Patterns: I used Singleton design pattern to make sure there will be only one instance of DatabaseClass object.

Additional tools: REST response testing - Postman, static analysis - Sonarlint( most suggestions fixed)

