package org.nowak.book_library.database;

import org.nowak.book_library.model.Author;
import org.nowak.book_library.model.Book;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class DatabaseClass {

    private static DatabaseClass instance = null;
    private  Map<String, Book> books;
    private  Map<String, Author> authors;

    public Map<String, Book> getBooks() {
        return books;
    }

    public Map<String, Author> getAuthors() {
        return authors;
    }

    private DatabaseClass() throws IOException {
        books = DatabaseLoader.loadBooks();
        authors = DatabaseLoader.loadAuthors(new ArrayList<>(books.values()));
    }

    public static DatabaseClass getInstance() throws IOException {

        if(instance == null) {
            instance = new DatabaseClass();
        }
        return instance;
    }

}
