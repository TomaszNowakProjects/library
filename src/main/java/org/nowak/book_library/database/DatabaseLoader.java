package org.nowak.book_library.database;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.nowak.book_library.deserializers.CustomBookDeserializer;
import org.nowak.book_library.model.Author;
import org.nowak.book_library.model.Book;

import java.io.File;
import java.io.IOException;
import java.util.*;

 class DatabaseLoader {

     private DatabaseLoader() {
     }

     protected static Map<String, Book> loadBooks() throws IOException {
        HashMap<String, Book> books = new HashMap<>();

        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule("CustomBookDeserializer", new Version(1, 0, 0, null, null, null));
        module.addDeserializer(ArrayList.class, new CustomBookDeserializer());
        mapper.registerModule(module);

         File file = new File(DatabaseLoader.class.getClassLoader().getResource("books.json").getFile());
        ArrayList<Book> booksList = mapper.readValue(file, ArrayList.class);

        for(Book b : booksList) {
            books.put(b.getIsbn(), b);
        }

        return books;
    }

    protected static Map<String, Author> loadAuthors(List<Book> books) {
        HashMap<String, Author> authors = new HashMap<>();
        HashSet<String> authorsNamesSet = new HashSet<>();

        for(Book book : books) {
            Double rating  = book.getAverageRating();

            ArrayList<String> bookAuthors =  book.getAuthors();
            if(bookAuthors != null) {
                for (String authorName : bookAuthors) {
                    if (authorsNamesSet.contains(authorName)) {
                            authors.get(authorName).updateRating(rating);
                    } else {
                        Author author = new Author(authorName);
                        authors.put(author.getName(), author);
                        authorsNamesSet.add(authorName);
                        authors.get(authorName).updateRating(rating);
                    }
                }
            }
        }

        return  authors;
    }
}
