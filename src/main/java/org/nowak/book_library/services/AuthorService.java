package org.nowak.book_library.services;

import org.nowak.book_library.database.DatabaseClass;
import org.nowak.book_library.exceptions.DatabaseException;
import org.nowak.book_library.model.Author;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class AuthorService {

    private Map<String, Author> authors;

    protected AuthorService(Map<String, Author> authors) {
        this.authors = authors;
    }

    public AuthorService() {
        try {
            DatabaseClass base = DatabaseClass.getInstance();
            authors = base.getAuthors();
        }
        catch (Exception e) {
            throw new DatabaseException("DatabaseException: " + e.getMessage());
        }
    }

    public List<Author> getAllAuthors() {
        return new ArrayList<>(authors.values());
    }

    public List<Author> getRating() {
        ArrayList<Author> rating =  new ArrayList<>();
        for(Author author : authors.values()) {
            if(author.getAverageRating() != null) {
                rating.add(author);
            }
        }

        Collections.sort(rating,Collections.reverseOrder());
        return rating;
    }
}
