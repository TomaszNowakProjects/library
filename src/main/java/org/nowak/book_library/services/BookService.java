package org.nowak.book_library.services;

import org.nowak.book_library.database.DatabaseClass;
import org.nowak.book_library.exceptions.DataNotFoundException;
import org.nowak.book_library.exceptions.DatabaseException;
import org.nowak.book_library.model.Book;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BookService {

    private Map<String, Book> books;

    protected BookService(Map<String, Book> books) {
        this.books = books;
    }

    public BookService() {
        try {
            DatabaseClass base = DatabaseClass.getInstance();
            books = base.getBooks();
        }
        catch (Exception e) {
            throw new DatabaseException("DatabaseException: " + e.getMessage());
        }
    }

    public List<Book> getAllBooks() {
        return new ArrayList<>(books.values());
    }

    public List<Book> getBooksByCategory(String category) {
        List<Book> booksByCategory = new ArrayList<>();
        for(Book book : books.values()) {
            ArrayList<String> categories = book.getCategories();
            if(categories != null) {
                for (String bookCategory : categories) {
                    if (bookCategory.equals(category)) {
                        booksByCategory.add(book);
                    }
                }
            }
        }
        return booksByCategory;
    }

    public Book getBook(String id) {
        Book book  = books.get(id);
        if(book == null)
            throw new DataNotFoundException("Book does not exist in the data set.");
        return book;
    }


}
