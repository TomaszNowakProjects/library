package org.nowak.book_library.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Author implements Comparable<Author>{

    @JsonProperty("author")
    private String name;
    @JsonIgnore
    private Double sumOfRatings = 0d;
    @JsonIgnore
    private Integer ratingCount = 0;
    private Double averageRating;

    public Author(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getSumOfRatings() {
        return sumOfRatings;
    }

    public void setSumOfRatings(Double sumOfRatings) {
        this.sumOfRatings = sumOfRatings;
    }

    public Integer getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(Integer ratingCount) {
        this.ratingCount = ratingCount;
    }

    public Double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }

    private void calculateAvgRating() {
        if (ratingCount > 0)
            averageRating = sumOfRatings/ratingCount;
    }

    public void updateRating(Double rate) {
        ratingCount = ratingCount + 1;
        if(rate != null && rate > 0) {
            sumOfRatings = sumOfRatings + rate;
            calculateAvgRating();
        }

    }
    @Override
    public int compareTo( Author o) {

        Double o1Rating = getAverageRating();
        Double o2Rating = o.getAverageRating();
        return o1Rating.compareTo(o2Rating);
    }

}
