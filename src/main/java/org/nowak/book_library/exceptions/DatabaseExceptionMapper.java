package org.nowak.book_library.exceptions;

import org.nowak.book_library.model.ErrorMessage;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DatabaseExceptionMapper implements ExceptionMapper<DatabaseException> {


    @Override
    public Response toResponse(DatabaseException e) {
        ErrorMessage errorMessage = new ErrorMessage(e.getMessage(),500);
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(errorMessage)
                .build();
    }
}
