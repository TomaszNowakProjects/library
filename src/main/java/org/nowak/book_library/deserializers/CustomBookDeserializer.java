package org.nowak.book_library.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.nowak.book_library.model.Book;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CustomBookDeserializer extends StdDeserializer<ArrayList<Book>> {

    public CustomBookDeserializer() {
        this(null);
    }

    public CustomBookDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public ArrayList<Book> deserialize(JsonParser parser, DeserializationContext deserializer) throws IOException {

        ObjectCodec codec = parser.getCodec();
        JsonNode node = codec.readTree(parser);
        ArrayList<Book> booksList= new ArrayList<>();

        ArrayNode itemsNode = (ArrayNode)node.get("items");

        if (itemsNode.isArray()) {
            for (final JsonNode objNode : itemsNode) {
                Book book = getItem(objNode);
                booksList.add(book);
            }
        }
        return booksList;
    }

    private Book getItem (JsonNode itemNode) {
        Book book = new Book();
        JsonNode volumeInfoNode = itemNode.path("volumeInfo");

        book.setIsbn(getIsbn(itemNode));
        book.setTitle(getTextValue(volumeInfoNode, "title"));
        book.setSubtitle(getTextValue(volumeInfoNode, "subtitle"));
        book.setPublisher(getTextValue(volumeInfoNode, "publisher"));
        book.setPublishedDate(getDate(volumeInfoNode));
        book.setDescription(getTextValue(volumeInfoNode,"description"));
        book.setPageCount(getIntValue(volumeInfoNode,"pageCount"));

        JsonNode imageLinksNode = volumeInfoNode.path("imageLinks");
        book.setThumbnailUrl(getTextValue(imageLinksNode,"thumbnail"));
        book.setLanguage(getTextValue(volumeInfoNode,"language"));
        book.setPreviewLink(getTextValue(volumeInfoNode,"previewLink"));
        book.setAverageRating(getDoubleValue(volumeInfoNode,"averageRating"));
        book.setAuthors(getArrayValue(volumeInfoNode, "authors"));
        book.setCategories(getArrayValue(volumeInfoNode, "categories"));

        return book;
    }

    private String getIsbn(JsonNode node) {

        ArrayNode industryIdentifiersNode = (ArrayNode)node.path("volumeInfo").get("industryIdentifiers");
        String id = null;
        if (industryIdentifiersNode.isArray()) {
            for (final JsonNode objNode : industryIdentifiersNode) {
                if(objNode.get("type").asText().equals("ISBN_13"))
                    id = objNode.get("identifier").asText();
            }

        }
        if(id==null)
            id = node.path("id").asText();

        return id;
    }

    private String getTextValue(JsonNode node, String valueName) {

        JsonNode value = node.get(valueName);
        if(value!=null) {
            return value.asText();
        }
        return null;
    }

    private ArrayList<String> getArrayValue(JsonNode node, String valueName) {
        JsonNode arrayNode = node.get(valueName);

            if (arrayNode != null && arrayNode.isArray()) {
                ArrayList<String> arrayValue = new ArrayList<>();
                for (final JsonNode objNode : arrayNode) {
                    arrayValue.add(objNode.asText());
                }
                return arrayValue;
            }

        return new ArrayList<>();
    }

    private Long getDate(JsonNode node) {
        JsonNode dateNode  = node.get("publishedDate");
        Long milliseconds = null;

        if(dateNode != null) {
            String stringDate = dateNode.asText();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date d = dateFormat.parse(stringDate);
                milliseconds = d.getTime();
            } catch (Exception e) {
                return null;
            }
        }
        return milliseconds;
    }

    private Integer getIntValue(JsonNode node, String valueName) {

        JsonNode value = node.get(valueName);
        if(value!=null) {
            return value.asInt();
        }
        return null;

    }

    private Double getDoubleValue(JsonNode node, String valueName) {

        JsonNode value = node.get(valueName);
        if(value!=null) {
            return value.asDouble();
        }
        return null;

    }


}
