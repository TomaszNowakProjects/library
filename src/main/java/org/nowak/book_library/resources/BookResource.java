package org.nowak.book_library.resources;


import org.nowak.book_library.model.Book;
import org.nowak.book_library.services.BookService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/books")
@Produces(MediaType.APPLICATION_JSON)
public class BookResource {

   private BookService bookService = new BookService();

    @GET
    public List<Book> getBooks(@QueryParam("category") String category) {

        if(category == null) {
            return bookService.getAllBooks();
        }

        return bookService.getBooksByCategory(category);
    }

    @GET
    @Path("/{bookId}")
    public Book getBook(@PathParam("bookId") String id) {
        return bookService.getBook(id);
    }


}
