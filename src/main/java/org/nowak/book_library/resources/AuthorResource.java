package org.nowak.book_library.resources;

import org.nowak.book_library.model.Author;
import org.nowak.book_library.services.AuthorService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/authors")
@Produces(MediaType.APPLICATION_JSON)
public class AuthorResource {

   private AuthorService authorService = new AuthorService();

    @GET
    public List<Author> getAuthors() {
        return authorService.getAllAuthors();
    }

    @GET
    @Path("/rating")
    public List<Author> getRating() {
        return authorService.getRating();
    }
}
