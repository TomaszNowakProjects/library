package org.nowak.book_library.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nowak.book_library.model.Author;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AuthorServiceTest {

    AuthorService testService;

    @Before
    public void setUp() {

        Author author1 = new Author("A");
        author1.updateRating(10d);
        Author author2 = new Author("B");
        author2.updateRating(20d);
        Author author3 = new Author("C");
        author3.updateRating(5d);
        Author author4 = new Author("D");

        Map<String, Author> database = new HashMap<>();
        database.put("A",author1);
        database.put("B",author2);
        database.put("C",author3);
        database.put("D",author4);
        testService = new AuthorService(database);
    }


    @Test
    public void ShouldGetAllAuthors() {
        List<Author> result = testService.getAllAuthors();
        Assert.assertEquals(4, result.size());
        Assert.assertEquals(Author.class,result.get(1).getClass());
    }

    @Test
    public void ShouldGetAllBooksEmpty() {
        testService = new AuthorService(new HashMap<String, Author>());
        List<Author> result = testService.getAllAuthors();
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void ShouldGetRating() {
        List<Author> result = testService.getRating();
        Assert.assertEquals(3, result.size());
        Assert.assertEquals(Author.class,result.get(1).getClass());
        Assert.assertEquals("B",result.get(0).getName());
        Assert.assertEquals("C",result.get(2).getName());
    }

    @Test
    public void ShouldGetRatingEmpty() {
        testService = new AuthorService(new HashMap<String, Author>());
        List<Author> result = testService.getRating();
        Assert.assertEquals(0, result.size());
    }

}
