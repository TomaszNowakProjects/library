package org.nowak.book_library.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nowak.book_library.exceptions.DataNotFoundException;
import org.nowak.book_library.model.Book;

import java.util.*;

public class BookServiceTest {

    BookService testService;

    @Before
    public void setUp() {
        Book bookOne = new Book();
        bookOne.setIsbn("1");
        bookOne.setCategories(new ArrayList<>(Arrays.asList("cat1")));
        Book bookTwo = new Book();
        bookTwo.setIsbn("2");
        bookOne.setCategories(new ArrayList<>(Arrays.asList("cat2")));
        Book bookThree = new Book();
        bookThree.setIsbn("3");
        bookThree.setCategories(new ArrayList<>(Arrays.asList("cat3","cat2")));

        Map<String, Book> database = new HashMap<>();
        database.put("1",bookOne);
        database.put("2",bookTwo);
        database.put("3",bookThree);
        testService = new BookService(database);
    }


    @Test
    public void ShouldGetAllBooks() {
        List<Book> result = testService.getAllBooks();
        Assert.assertEquals(3, result.size());
        Assert.assertEquals(Book.class,result.get(1).getClass());
    }

    @Test
    public void ShouldGetAllBooksEmpty() {
        testService = new BookService(new HashMap<String, Book>());
        List<Book> result = testService.getAllBooks();
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void getBooksByCategory() {
        List<Book> result = testService.getBooksByCategory("cat2");
        Assert.assertEquals(2, result.size());
        Assert.assertEquals(Book.class,result.get(1).getClass());
    }

    @Test
    public void getBooksByCategoryEmpty() {
        List<Book> result = testService.getBooksByCategory("test");
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void getBook() {
        Book result = testService.getBook("1");
        Assert.assertEquals(Book.class,result.getClass());
        Assert.assertEquals("1",result.getIsbn());
    }

    @Test(expected = DataNotFoundException.class)
    public void getBookEmpty() {
        Book result = testService.getBook("11");
    }

}