package org.nowak.book_library.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AuthorTests {

    Author author1;
    Author author2;

    @Before
    public void setUp() {
        author1 = new Author("test1");
        author2 = new Author("test2");
    }

    @Test
    public void shouldUpdateRating() {
        author1.updateRating(20d);
        author1.updateRating(10d);
        Double expected = 15d;
        assertEquals(expected,author1.getAverageRating());
    }

    @Test
    public void shouldUpdateRatingNULL() {
        author1.updateRating(null);
        Double expected = null;
        assertEquals(expected,author1.getAverageRating());
    }

    @Test
    public void shouldUpdateRatingNegativeNumber() {
        author1.updateRating(10d);
        author1.updateRating(-12.45d);
        Double expected = 10d;
        assertEquals(expected,author1.getAverageRating());
    }

    @Test
    public void shouldCompareBigger() {
        author1.updateRating(100d);
        author2.updateRating(10d);
        Integer compareResult = author1.compareTo(author2);
        Integer expected = 1;
        assertEquals(expected,compareResult);
    }

    @Test
    public void shouldCompareSmaller() {
        author1.updateRating(10d);
        author2.updateRating(100d);
        Integer compareResult = author1.compareTo(author2);
        Integer expected = -1;
        assertEquals(expected,compareResult);
    }

    @Test
    public void shouldCompareEquals() {
        author1.updateRating(10d);
        author2.updateRating(10d);
        Integer compareResult = author1.compareTo(author2);
        Integer expected = 0;
        assertEquals(expected,compareResult);
    }


}
